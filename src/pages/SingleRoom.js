import React, { Component } from 'react';
import defaultBCG from '../images/room-1.jpeg';
//import DynamicHeader from '../components/DynamicHeader';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import {RoomContext } from '../context';
import StyledHeaderImgAuto from '../components/StyledDynamicImgHeader';
import { FaRupeeSign } from 'react-icons/fa';


export default class SingleRoom extends Component {

    constructor(props) {
        super(props);
        this.state = {
            param: this.props.match.params.s,   //here in params we take 's' because you mention this url in app.js
            defaultBCG,
        };
        //console.log(this.props.match.params.s);  
    }

    static contextType = RoomContext;
    //componentDidMount() {}
 
    render(){
        //const { getRoom } = this.context;
        //console.log("Thisis: ");
        const room = this.context.getRoom(this.state.param);
        if(!room) {
            return <div className="error">
                <h3>Sorry no rooms Avalible Currently visit again !</h3>
                <Link to="/rooms" className="btn-primary">Back to Rooms</Link>
            </div>
        }
            //here no required else because in 'if' we use return

            //we use this concept because when first time or initially this page load the below return and we don't have any value to render so it gives error
            //thus we check if room is empty then we render above means we don't have any data
            //but as soon as we get data we value of room changes the render funtion executes and now we have data in room so we execute below return 

        const { name, description, capacity, size, price, extras, breakfast, pets, images} = room;
        //so for an section we dont want banner image so
        const [firstImg,...allImg] = images;
        //console.log(extras);
        return (
            <>
                <StyledHeaderImgAuto img={firstImg || this.state.defaultBCG} >
                    <Banner title = {`${name} room`} childern = {
                        <Link to="/rooms" className = "btn-primary">
                            Back to Rooms
                        </Link>
                    } />
                </StyledHeaderImgAuto>
                <section className="single-room">
                    <div className="single-room-images">
                        {allImg.map((item, index) => {
                            return <img key={index} src={item} alt={name} />
                        })}
                    </div>

                    <div className="single-room-info">
                        <article className="desc">
                            <h3>Details</h3> 
                            <p>{ description }</p>
                        </article>
                        <article className="info">
                            <h3>Info</h3>
                            <h6> Price: <span className="rupee_icon"><FaRupeeSign /></span> {price}</h6>
                            <h6> Size: {size} sqft.</h6>
                            <h6> Max Capacity: {
                                capacity > 1 ? `${capacity} people` : `${capacity} person` 
                            }</h6>
                            <h6>  { pets? "Pets are Allowed":"Not pets Allowed" }</h6>
                            <h6>{ breakfast && "Free Breakfast Included"}</h6>  {/* remember this AND condition and improved logic */}
                            
                        </article>
                    </div>
                </section>
                <section className="room-extras">
                    <h6>Extras</h6>
                    <ul className="extras">
                        { extras.map((item, index) => {
                            return <li key={index}> -{item} </li>
                        })}
                    </ul>
                </section>



            </>
        )
    }
}
