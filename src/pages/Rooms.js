import React from 'react';
import DynamicHeader from '../components/DynamicHeader';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom'
import RoomContainer from '../components/RoomContainer';
const Rooms = () => {
    return (
       <>
        <DynamicHeader headerClass="roomsHero" childern={
            <Banner title="Our Rooms" subtitle="" childern={
                <Link to="/" className="btn-primary">
                    Return Home
                </Link>
            }/>
        } />
        <RoomContainer />
     </>
    )
}


export default Rooms;