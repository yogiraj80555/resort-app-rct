import React from 'react'
import DynamicHeader from '../components/DynamicHeader';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
import Services from '../components/Services';
import FeaturedRooms from '../components/FeaturedRooms';



export default function Home () {

    return (
        <React.Fragment>
            <DynamicHeader
                childern = {<Banner title="Luxurious Rooms" subtitle={`Deluxe room Starting @ ${'\u20B9'}2598`}
                        childern = {<Link to="/rooms" className="btn-primary">
                                        Our Rooms
                                    </Link>}
                        />} 
            />

            <Services/>
            <FeaturedRooms />
            
        </React.Fragment>
        )            
        
        
        
    
}



