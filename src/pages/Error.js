import React from 'react';
import DynamicHeader from '../components/DynamicHeader';
import Banner from '../components/Banner';
import {Link} from 'react-router-dom';
const Error = () => {
    return <DynamicHeader childern={
        <Banner title="Error 404" subtitle="Page Not Found" childern={
            <Link to="/" className="btn-primary">
                Home 
            </Link>
        } />
    } />
}


export default Error;