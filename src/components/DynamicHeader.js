import React from 'react';

export default function Hero ({childern, headerClass})  {

    return <header className={headerClass}>{childern}</header> 

}

Hero.defaultProps={
    headerClass : "defaultHero",
}