import React, { Component } from 'react';
import {RoomContext} from '../context';
import Loading from './Loading';
import Room from './Room';
import Title from './Title';



class FeaturedRooms extends Component {

    static contextType = RoomContext;


//Hello From {this.context.name} Rooms Featured {this.context.greetings}
    render(){
        const {loading ,featuredRooms : fetRooms} = this.context;
        //console.log("Current Context is: ");
        //console.log(fetRooms);
        const rooms = fetRooms.map(room => {
            return <Room key={room.id} room={room} />
        });


        return(
        <section className="featured-rooms">
            <Title title="Featured rooms" />
            {/*Be careful with loading if it fast and rooms are empty it directly generated error and this is runtime error
            it will directly affect your project presentation and leads bug */}
            <div className="featured-rooms-center">
                {loading? <Loading />:rooms}
            </div>
          
        </section>
        )
    }
}


export default FeaturedRooms;