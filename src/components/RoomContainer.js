import React from 'react'
import { withRoomConsumer } from '../context'
import Loading from './Loading';
import RoomFilter from './RoomFilter';
import RoomList from './RoomList';

function RoomContainer({ context }) {

    const {loding, sortedRooms, rooms } = context;
    if(loding){
        return <Loading />
    }
    return (
        <>
            <RoomFilter  rooms={ rooms } />
            <RoomList rooms={ sortedRooms } />
        </>
    )
}


export default withRoomConsumer(RoomContainer);

















//below is an normal way to use consumer in context the upword is we are using higher order component for this i have taken help of hooks
//for the refrance see 'withRoomConsumer' function in context.js i used this to achive below functionlity.
//but again which is effcient is up to you which way really you want to go....  and that's all. 




/*import React from 'react'
import RoomFilter from './RoomFilter';
import RoomList from './RoomList';
import { RoomConsumer } from '../context.js'
import Loading from './Loading';


export default function RoomContainer() {
    return (
        //i thought this is a efficient way of context consumer when we use functional component
        <RoomConsumer>
            {
                value => {
                    //console.log(value);
                    const {loding, sortedRooms, rooms} = value;
                    if(loding){
                        return <Loading />
                    }
                    return (
                        <div>
                            Hello from Room Container
                            <RoomFilter room = {rooms}/>
                            <RoomList sortedroom={sortedRooms} />
                        </div>
                    );
                }
            }



        </RoomConsumer>

        
    )
}
*/