import React, { Component }from 'react';
import Title from './Title';
import {FaCocktail, FaHiking, FaShuttleVan, FaBeer} from 'react-icons/fa';
export default class Services extends Component {

    state = {
        service:[
            {
                icon: <FaCocktail />,
                title: "Unlimited Cocktails",
                info: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
            },
            {
                icon: <FaHiking />,
                title: "Endless Hicking",
                info : 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'

            },
            {
                icon: <FaShuttleVan />,
                title: "Free Shuttle",
                info: 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. '

            },
            {
                icon: <FaBeer />,
                title: "strongest Beer",
                info: 'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. '
            }
        ]
    }

    render(){
        return (
            <section className="services">
                <Title title="Services" />
                <div className="services-center">
                    {this.state.service.map((item,keys) =>{
                        return <article key={keys} className="service">
                            <span>{item.icon}</span>
                            <h6>{item.title}</h6>
                            <p>{item.info}</p>
                        </article>
                    })}
                </div>
            </section>
        )
    }
}