import React from 'react';
import lodingGif from '../images/gif/ground.gif';

export default function Loading(){

    return(
        <div className="loading">
            <h4> Loading .... </h4>
            <img src={lodingGif} alt="Waiting ...." />
        </div>
    )
}