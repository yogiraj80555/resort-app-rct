import React from 'react'
import { useContext } from 'react'; //it's an latest way to use context and this using an hooks
import { RoomContext } from '../context';
import Title from './Title';


const getUnique = (rooms, value) => {
    return [...new Set(rooms.map(item => item[value]))]
}

export default function RoomFilter({rooms}) {

    const context = useContext(RoomContext);
    //console.log(context)
    const {handleChange, price, minPrice, maxPrice, type, capacity, minSize, maxSize, pets, breakfast} = context;
    //console.log(type)
    //console.log(getUnique(rooms,'type'));
    //console.log(getUnique(rooms, 'capacity'))
    let types = ['all',...getUnique(rooms,'type')];
    types = types.map((item,index) =>{
       return <option value={item} key={index}>{item}</option>
    })



    let capacitys = [...getUnique(rooms,'capacity')];
    capacitys = capacitys.map((item,index)=>{
    return <option value={item} key={index}> {item} </option>
    })
    
    return (
        <section className="filter-container">
            <Title title="Search Rooms" />
            <form className="filter-form">
                {/* Select Type */}
                <div className="form-group">
                    <label htmlFor="type">Room Type</label>
                    <select name="type" id="type" value={type} className="form-control" onChange={handleChange}>
                    { types }
                    </select>
                </div>


                {/* Select guest capacity*/}
                <div className="form-group">
                    <label htmlFor="Capacity">Guests</label>
                    <select name="capacity" id="capacity" value={capacity} className="form-control" onChange={handleChange}>
                    {capacitys}
                    </select>
                </div>


                {/* For Money range */}
                <div className="form-group">
                    <label htmlFor="price">Room Price {'\u20B9'+price}</label>
                    <input type="range" name="price" id="price" className="form-control" value={price} min={minPrice} max={maxPrice} onChange={handleChange} />
                </div>

                {/* for room size */}
                <div className="form-group">
                    <label htmlFor="size">Room Size</label>
                    <div className="size-inputs">
                        <input type="number" name="minSize" id="size" value={minSize} onChange={handleChange} className="size-input"/>
                        <input type="number" name="maxSize" id="size" value={maxSize} onChange={handleChange} className="size-input"/>
                    </div>
                </div>



                {/* */}
                <div className="form-group">
                    <div className="single-extra">
                        <input type="checkbox" name="breakfast" id="brekfast" checked={breakfast} onChange={handleChange} />
                        <label htmlFor="Breakfast">Breakfast</label> 
                    </div>
                    <div className="single-extra">
                        <input type="checkbox" name="pets" id="pets" checked={pets} onChange={handleChange} />
                        <label htmlFor="pets">Pets</label>
                    </div>
                </div>
            </form>
        </section>
    )
}
