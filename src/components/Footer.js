import React from 'react'



export default function Footer() {


    //console.log(Bootstrap);
    return (
        <>
            <footer className="footer">
                <div classname="">
                    <ul className="footer-containt">
                        <li>This Project is created and maintained by <a href="http://www.yogiraj.ml">Yogiraj Patil</a></li>
                        <li>You will find an project Repository <a href="https://gitlab.com/yogiraj80555/resort-app-rct">here.</a></li>
                        <li>See more project's <a href="https://gitlab.com/yogiraj80555/">click here</a></li>
                    </ul>
                    <p>Any Query and Suggestion please feel free to contact me on: <a href="mailto:yogiraj.218m0064@viit.ac.in">Yogiraj Mail</a>. </p>
                    <p>Thankyou.</p>
                    </div>
            </footer>
        </>
    )
}
