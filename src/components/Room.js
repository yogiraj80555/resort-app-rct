import React from 'react';
import { Link } from 'react-router-dom';
import defaultImg from '../images/room-2.jpeg'
import PropTypes from 'prop-types';
export default function Room({room}){

    const {name,images,slug,price} = room;

    return(
        <article className="room">
            <div className="img-container">
                <img src={images[0] || defaultImg} alt="Room View" height="190px"  />
                <div className="price-top">
                    <h6>{'\u20B9'+price}</h6>
                    <p>per night</p>
                </div>
                <Link to={`/rooms/${slug}`} className="btn-primary room-link">Features</Link>
            </div>
            <p className="room-info">{name}</p>
            
        </article>
    )
}


//so if room cantain any invalid data or null it generate warning in console

Room.propTypes = {
    room: PropTypes.shape({
        name: PropTypes.string.isRequired,
        slug: PropTypes.string.isRequired,
        images: PropTypes.arrayOf(String).isRequired,
        price: PropTypes.number.isRequired,

    })
}