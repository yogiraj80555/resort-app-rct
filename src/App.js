import React from 'react';
import './App.css';
import Home from './pages/Home';
import Rooms from './pages/Rooms';
import SingleRoom from './pages/SingleRoom';
import Error from './pages/Error';
import Footer from './components/Footer';

import Navbar from './components/Navbar';

import { Route, Switch } from 'react-router-dom';

function App() {
  return (
  <>
    <Navbar />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/rooms" component={Rooms} />
      <Route exact path="/rooms/:s" component={SingleRoom} />
      <Route component={Error} />
    </Switch>
    <Footer />
  </>
  );
}

export default App;
