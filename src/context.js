import React, { Component } from 'react';
//import items from './data';
import Client from './Contentful';

/*Client.getEntries({
    content_type: 'resortRoom',
}).then(response => console.log(response.items));
*/

const RoomContext = React.createContext();
  // so this wolud be provider
class RoomProvider extends Component {

    state = {
        rooms: [],
        sortedRooms: [],
        featuredRooms: [],
        loding: true,
        type: 'all',
        capacity: 1,
        price: 0,
        minPrice: 0,
        maxPrice: 0,
        minSize:0,
        maxSize: 0,
        breakfast: false,
        pets: false,
    };

    getData = async () => {

       

        try{
            let response = await Client.getEntries({
                content_type: 'resortRoom',
                //order: "sys.createdAt",
                order: "fields.price"   //<- it's an order by json property
                //order: "-fields.price"  //it's an reverse order

            });
            
            //console.log(response);

            //let rooms = this.formatData(items);
            let rooms = this.formatData(response.items);

            //console.log("Current Rooms");
            //console.log(rooms);
            let featuredRooms = rooms.filter(room => room.featured === true);
            //let sortedRooms = rooms.sort((a,b) => a.price - b.price);
            //console.log(featuredRooms);

            const maxPrice = Math.max(...rooms.map(item => item.price));
            const maxSize = Math.max(...rooms.map(items => items.size))
            //console.error("Max Price is:"+maxPrice);
            //console.error("Max Size is: "+maxSize);

            this.setState({
                //here state and function variable name is same so this is fine
                rooms,
                featuredRooms,
                sortedRooms: rooms,
                loding: false,
                price : maxPrice,
                maxPrice,
                maxSize,
            });
   
        }catch(error){
            console.log(error);
        }
    }

    componentDidMount(){
        //console.log("Token"+process.env.REACT_APP_RESORT_APP_API_ACCESS_TOKEN);
        this.getData();
    }

    formatData(items){
        let tempItems = items.map(item =>{
            let id = item.sys.id;
            let images = item.fields.images.map(image => image.fields.file.url);
            let room ={...item.fields, images, id};
            
            return room;
        });
        return tempItems
    }

    getRoom = (slug) => {
        let tempRooms = [...this.state.rooms]; 
        const room = tempRooms.find( room => room.slug === slug);//here we use 'find' beacause we just want first matching room if you want all matching room then you may use 'filter' instead of 'find'
        return room;
    }

    handleChange = event => {
        const target = event.target.type;
        const name = event.target.name;
        let value = event.target.value;
        //console.log("From HandleChange");
        //console.log(target,name,value);
        
        if(target === "checkbox"){
            //console.log("We entered "+event.target.checked);
            value = event.target.checked
            
        }


        this.setState({[name]: value, },this.filterRooms);
       
    };

    filterRooms = () => {
        //console.log("Sorted");
        let { rooms, type, capacity, price, maxPrice, minSize, maxSize, breakfast, pets} = this.state;
        //console.log(sortedRooms)
        let tempRooms = [...rooms];
        //console.log(rooms);
        //filter by type's
        if(type !== 'all'){
            tempRooms = tempRooms.filter(room => room.type === type);
        }

        //filering by capacity
        if(capacity !== 1){  //here you can take parseInt(capacity) also to typecast in integer because event convert capacity to string therefor '===' i not used
            tempRooms = tempRooms.filter(room => room.capacity >= capacity)
            //console.log(tempRooms);
        }

        //filtering by price
        price = parseInt(price);
        if(price !== maxPrice){
            tempRooms = tempRooms.filter(room => room.price <= price)
            //console.log("MaxPrice entered"+ price+ "  "+maxPrice); 
        }

        //filtering free breakfast
        if(breakfast){
            tempRooms = tempRooms.filter(room => room.breakfast === true)
        }

        //filtering pets allowed rooms
        if(pets){
            tempRooms = tempRooms.filter(room => room.pets === true)
        }



        //filtering by size
        tempRooms = tempRooms.filter(room => room.size >= minSize  &&  room.size <= maxSize)

        this.setState({sortedRooms:tempRooms})
    };

  
    render(){
        return <RoomContext.Provider value={{...this.state, getRoom: this.getRoom, handleChange: this.handleChange}}>
            {this.props.children}
        </RoomContext.Provider>
    }
}


const RoomConsumer = RoomContext.Consumer;

export function withRoomConsumer (Component){
    return function ConsumerWrapper(props){
        return(
            <RoomConsumer>
            {
                value => <Component {...props} context={value} />
            }
            </RoomConsumer>

        )
    }
}


export {RoomContext, RoomProvider, RoomConsumer}