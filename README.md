This is an Hotel room Reservation app created by me during exploring react and thanks for free coding camp for giving such simple guideline's of tutorial. it's a lot's appreciated.
The Project has been developed using netlify and contentfull api appriciated there free service and support

To See Running project <a href="https://yogiraj-resort-app.netlify.com/">Click here</a> 

Below is an some Images of project:
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<figure class="figure mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/ReactResortApp/home.JPG" width="400" height="380" class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>Home Page </b></i></figcaption>
</figure>

<br/>

<figure class="figure mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/ReactResortApp/roominfo.JPG" width="400" height="380" class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>Single Room Information and Available Facility's </b></i></figcaption>
</figure>

<br/>

<figure class="figure mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/ReactResortApp/listofrooms.JPG" width="400" height="380" class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>Types of All Rooms</b></i></figcaption>
</figure>

<br/>


<figure class="figure mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/ReactResortApp/room filters.JPG" width="400" height="380" class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>Avalible Filter's to find specific Room</b></i></figcaption>
</figure>

<br/>
<br/>



The Project has been developed using netlify and contentfull api appriciated there free service and support <br/>
For any query, questions and suggestions please feel free to contact on below mail id.<br/>
Thankyou. <br/>
By Yogiraj Patil <br/>
Mail: yogiraj.218m0064@viit.ac.in
To Know <a href="http://www.yogiraj.ml">About me</a>   